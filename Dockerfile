FROM node:16.19.0

ENV TZ Europe/Amsterdam

WORKDIR /app

COPY . .

RUN npm ci

EXPOSE 3000

CMD ["npm", "run", "start"]
