const mongoose = require("mongoose");
const { checkToken } = require("./checkTokens");

const createSort = (req) =>
  req.query.sort
    ? {
        [JSON.parse(req.query.sort)[0]]:
          JSON.parse(req.query.sort)[1] === "ASC" ? 1 : -1,
      }
    : { $natural: -1 };

const createRange = (req, total) =>
  req.query.range ? JSON.parse(req.query.range) : [0, total];

const createFilter = (req) =>
  req.query.filter ? JSON.parse(req.query.filter) : {};

const createProductQuery = (mongooseModel, req) => {
  const filter = createFilter(req);
  const baseQuery = mongooseModel.find();

  if (Object.keys(filter).length !== 0) {
    const filters = filter.id.map((filter) => mongoose.Types.ObjectId(filter));
    baseQuery.where("_id").in(filters);
  }

  return baseQuery;
};

const createOrdersQuery = (mongooseModel, req) => {
  const filter = createFilter(req);
  const baseQuery = mongooseModel.find();

  if (filter.id) {
    const filters = filter.id.map((filter) => mongoose.Types.ObjectId(filter));
    baseQuery.where("_id").in(filters);
  }

  if (filter.isVerified) {
    baseQuery.where("isVerified").equals(filter.isVerified);
  }

  if (filter.status) {
    baseQuery.where("status").equals(filter.status);
  }

  if (filter.uuid !== "*") {
    baseQuery.where("client").equals(filter.uuid);
  }

  return baseQuery;
};

const createBaseQuery = (mongooseModel, req) => {
  const filter = createFilter(req);

  return Object.entries(filter).reduce((acc, [key, value]) => {
    if (key === "id") {
      const filters = filter.id.map((filter) =>
        mongoose.Types.ObjectId(filter)
      );
      acc.where("_id").in(filters);

      return acc;
    }

    acc.where(key).equals(value);

    return acc;
  }, mongooseModel.find());
};

const addCreateOne = (mongooseModel, routeName) => (app) => {
  app.post(`/${routeName}`, checkToken, async (req, res) => {
    const { data } = req.body;

    const resp = await mongooseModel.create(data);

    return res.status(200).json({
      data: resp,
    });
  });
};

const addFindOne = (mongooseModel, routeName) => (app) => {
  app.get(`/${routeName}/:id`, checkToken, async (req, res) => {
    const data = await mongooseModel.findById(req.params.id).exec();
    return res.status(200).json({ data });
  });
};

const addFindMany = (mongooseModel, routeName, baseQuery) => (app) => {
  app.get(`/${routeName}`, checkToken, async (req, res) => {
    const total = await mongooseModel.countDocuments();

    const [skip, limit] = createRange(req, total);
    const sort = createSort(req);

    const data = await baseQuery(mongooseModel, req)
      .sort(sort)
      .skip(skip)
      .limit(limit)
      .exec();

    return res.status(200).json({ data, total });
  });
};

const addUpdateMany = (mongooseModel, routeName) => (app) => {
  app.put(`/${routeName}/:id`, checkToken, async (req, res) => {
    const data = await mongooseModel
      .updateMany(
        { _id: req.params.id },
        {
          $set: { ...req.body.data, lastModified: Date.now() },
        }
      )
      .exec();

    return res.status(200).json({ data });
  });
};

const addReplaceOne = (mongooseModel, routeName) => (app) => {
  app.put(`/${routeName}/:id`, checkToken, async (req, res) => {
    const update = req.body.data;

    const data = await mongooseModel
      .replaceOne(
        { _id: req.params.id },
        {
          ...update,
          lastModified: Date.now(),
        }
      )
      .exec();

    return res.status(200).json({ data });
  });
};

const addDeleteMany = (mongooseModel, routeName) => (app) => {
  app.delete(`/${routeName}/:id`, checkToken, async (req, res) => {
    const data = await mongooseModel
      .deleteMany({
        _id: JSON.parse(req.query.filter).id,
      })
      .exec();
    return res.status(200).json({ data });
  });
};

const addDeleteOne = (mongooseModel, routeName) => (app) => {
  app.delete(`/${routeName}/:id`, checkToken, async (req, res) => {
    const data = await mongooseModel.deleteOne({ _id: req.params.id }).exec();
    return res.status(200).json({ data });
  });
};

const addRoutes =
  ({ model, routeName, baseQuery = createBaseQuery, routeNameBulk }) =>
  (app) => {
    const bulkRoute = routeNameBulk ? routeNameBulk : `${routeName}s`;

    addCreateOne(model, routeName)(app);
    addFindOne(model, routeName)(app);
    addFindMany(model, bulkRoute, baseQuery)(app);
    addUpdateMany(model, bulkRoute)(app);
    addReplaceOne(model, routeName)(app);
    addDeleteMany(model, bulkRoute)(app);
    addDeleteOne(model, routeName)(app);
  };

module.exports = {
  addRoutes,
  createOrdersQuery,
  createProductQuery,
};
