const jwt = require("jsonwebtoken");
const config = require("../config");

const getTokenFromRequest = (req) =>
  req.headers["x-access-token"] || req.headers["authorization"];

const stripToken = (token) => token.slice(7, token.length);

const validateToken = (req, next, token) =>
  jwt.verify(stripToken(token), config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).json({
        success: false,
        message: "Token is not valid",
      });
    }

    req.decoded = decoded;
    next();
  });

const checkToken = (req, res, next) => {
  const token = getTokenFromRequest(req);

  if (!token.startsWith("Bearer ")) {
    return res.status(401).json({
      success: false,
      message: "Auth token is not supplied",
    });
  }

  validateToken(req, next, token);
};

module.exports = {
  checkToken,
  stripToken,
  getTokenFromRequest,
  validateToken,
};
