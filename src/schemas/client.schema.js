const mongoose = require("mongoose");

const clientSchema = new mongoose.Schema({
  name: String,
  zipcode: String,
  address: String,
  housenumber: Number,
  isActive: { type: Boolean, default: false },
});

const Client = mongoose.model("Client", clientSchema, "clients");

module.exports = {
  clientSchema,
  Client,
};
