const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  products: mongoose.Schema.Types.Mixed,
  client: String,
  orderer: String,
  orderDate: { type: Date, default: Date.now },
  lastModified: { type: Date, default: Date.now },
  status: {
    type: String,
    default: "In behandeling",
  },
  isVerified: {
    type: Boolean,
    default: false,
  },
});

const Order = mongoose.model("Order", orderSchema, "orders");

module.exports = {
  orderSchema,
  Order,
};
