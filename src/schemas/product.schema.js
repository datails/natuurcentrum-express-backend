const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: String,
  description: String,
  quantity: Number,
  quantityTypes: [
    {
      name: String,
      quantity: Number,
      id: mongoose.Schema.Types.Mixed,
    },
  ],
  harvestBeforeSummer: {
    type: Boolean,
    default: false,
  },
  harvestAfterSummer: {
    type: Boolean,
    default: false,
  },
  leaf: {
    type: Boolean,
    default: false,
  },
  wortelOrKnol: {
    type: Boolean,
    default: false,
  },
  seed: {
    type: Boolean,
    default: false,
  },
  price: Number,
  category: mongoose.Schema.Types.Mixed,
  date: { type: Date, default: Date.now },
  isActive: { type: Boolean, default: false },
  pictures: {
    src: String,
    rawFile: mongoose.Schema.Types.Mixed,
  },
});

const Product = mongoose.model("Product", productSchema, "products");

module.exports = {
  productSchema,
  Product,
};
