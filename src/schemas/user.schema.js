const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  name: String,
  email: String,
  password: String,
  isAdmin: { type: Boolean, default: false },
  isActive: { type: Boolean, default: false },
  client: String,
});

const User = mongoose.model("User", userSchema, "users");

module.exports = {
  userSchema,
  User,
};
