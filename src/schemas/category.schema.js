const mongoose = require("mongoose");

const categorySchema = new mongoose.Schema({
  name: String,
  description: String,
  color: mongoose.Schema.Types.Mixed,
});

const Category = mongoose.model("Category", categorySchema, "categories");

module.exports = {
  categorySchema,
  Category,
};
