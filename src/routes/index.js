const {
  addRoutes,
  createProductQuery,
  createOrdersQuery,
} = require("../utils/createRoutes");
const { Category } = require("../schemas/category.schema");
const { User } = require("../schemas/user.schema");
const { Client } = require("../schemas/client.schema");
const { Order } = require("../schemas/order.schema");
const { Product } = require("../schemas/product.schema");
const { createLoginRoute } = require("./login");

const createCategoryRoutes = addRoutes({
  model: Category,
  routeName: "category",
  routeNameBulk: "categories",
});

const createClientRoutes = addRoutes({
  model: Client,
  routeName: "client",
});

const createOrderRoutes = addRoutes({
  model: Order,
  routeName: "order",
  baseQuery: createOrdersQuery,
});

const createProductRoutes = addRoutes({
  model: Product,
  routeName: "product",
  baseQuery: createProductQuery,
});

const createUserRoutes = addRoutes({
  model: User,
  routeName: "user",
});

const createRoutes = function (app) {
  createCategoryRoutes(app);
  createClientRoutes(app);
  createLoginRoute(app);
  createOrderRoutes(app);
  createProductRoutes(app);
  createUserRoutes(app);
};

module.exports = createRoutes;
