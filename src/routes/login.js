const jwt = require("jsonwebtoken");
const { User } = require("../schemas/user.schema");
const config = require("../config");

const hasUserNamePassword = (body) => body.username && body.password;

const isUserAuthenticated = (body, user) =>
  user &&
  body.username === user.email &&
  body.password === user.password &&
  user.isActive;

const createToken = (user) =>
  jwt.sign(
    {
      name: user.name,
      email: user.email,
      permissions: user.isAdmin ? "admin" : "editor",
      id: user._id,
      client: user.client,
    },
    config.secret,
    {
      expiresIn: "24h",
    }
  );

const createLoginRoute = function (app) {
  app.post("/login", async (req, res) => {
    const body = JSON.parse(req.body);

    if (!hasUserNamePassword(body)) {
      return res.status(400).json({
        success: false,
        message: "Authentication failed! Please check the request",
      });
    }

    const user = await User.findOne({
      email: body.username,
    }).exec();

    if (!isUserAuthenticated(body, user)) {
      return res.status(401).json({
        success: false,
        message: "Incorrect username or password",
      });
    }

    return res.status(200).json({
      success: true,
      token: createToken(user),
    });
  });
};

module.exports = {
  createLoginRoute,
};
