const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const appConfig = require("./config");

const app = express();

app.use(cors({ credentials: true, origin: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text());
app.use(bodyParser.json());

mongoose.connect(appConfig.dbUri, {
  w: "majority",
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

require("./routes/index")(app);

app.listen(appConfig.port);
