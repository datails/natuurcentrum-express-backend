const appConfig = {
  secret: process.env.JWT_SECRET,
  dbUri: process.env.MONGO_URI,
  port: process.env.PORT || 4000,
  host: process.env.HOST,
};

module.exports = appConfig;
