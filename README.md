# Natuurcentrum Arnhem Backend

## Development
Make sure docker is installed. Next run:

```bash
docker-compose up
```

Now you can open the API at `localhost:3000` and the mongo ui at `localhost:8081`.

## Deployment
Deployment goes to heroku.

```bash
heroku login
heroku git:remote -a [NAME_OF_PROJECT]
git add .
git commit -m "nice msg"
git push heroku master
```